/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "PropertyBelief.h"
#include "Util.h"

using namespace AffordanceKit;

float PropertyBelief::lambdaSpatial = 0.5;
float PropertyBelief::lambdaRotational = 20;

int PropertyBelief::sigmoidLookupTableLowerBound = -10;
int PropertyBelief::sigmoidLookupTableUpperBound = 10;
int PropertyBelief::sigmoidLookupTableResolution = 1000;

Eigen::MatrixXf PropertyBelief::sigmoidLookupTable = GenerateSigmoidLookupTable();

Eigen::Vector3f PropertyBelief::up = Eigen::Vector3f::UnitZ();

Eigen::MatrixXf PropertyBelief::GenerateSigmoidLookupTable()
{
    int size = (int)((sigmoidLookupTableUpperBound - sigmoidLookupTableLowerBound) * sigmoidLookupTableResolution);
    Eigen::MatrixXf lookupTable(2, size);

    for (int i = 0; i < size; i++)
    {
        float x = sigmoidLookupTableLowerBound + i / ((float)sigmoidLookupTableResolution);
        lookupTable(0, i) = 1.0f / (1.0f + exp(-lambdaSpatial * x));
        lookupTable(1, i) = 1.0f / (1.0f + exp(-lambdaRotational * x));
    }

    return lookupTable;
}

Belief PropertyBelief::Horizontal(const PrimitivePtr& primitive, unsigned int index, const EmbodimentPtr& embodiment, float certainty)
{
    const float epsilon = M_PI / 4;

    Eigen::Vector3f z = -primitive->getSampling(index).block<3, 1>(0, 2);
    float cos_angle = up.dot(z) / z.norm();
    CLAMP_TO_ACOS_DOMAIN(cos_angle);

    return InThresholdIntervalBelief(acos(cos_angle), -epsilon, epsilon, eRotational, certainty);
}

AffordanceKit::Belief AffordanceKit::PropertyBelief::Horizontal(const Eigen::Vector3f& direction, const AffordanceKit::EmbodimentPtr& embodiment, float certainty)
{
    const float epsilon = M_PI / 4;

    float cos_angle = up.dot(direction) / direction.norm();
    CLAMP_TO_ACOS_DOMAIN(cos_angle);

    return InThresholdIntervalBelief(acos(cos_angle), M_PI / 2 - epsilon, M_PI / 2 + epsilon, eRotational, certainty);
}

Belief PropertyBelief::Vertical(const Eigen::Matrix4f& pose, const EmbodimentPtr& embodiment, float certainty)
{
    const float epsilon = M_PI / 4;

    Eigen::Vector3f z = -pose.block<3, 1>(0, 2);
    float cos_angle = up.dot(z) / z.norm();
    CLAMP_TO_ACOS_DOMAIN(cos_angle);

    return InThresholdIntervalBelief(acos(cos_angle), M_PI / 2 - epsilon, M_PI / 2 + epsilon, eRotational, certainty);
}

Belief PropertyBelief::Circular(const PrimitivePtr& primitive, float certainty)
{
    const float betaCircular = 0.8;
    return GreaterThanThresholdBelief(primitive->isCircular(), betaCircular, eRotational, certainty);

}

Belief PropertyBelief::Fixed(const PrimitivePtr& primitive, const EmbodimentPtr& embodiment, float certainty)
{
    Eigen::Vector3f d = primitive->getDimensions();

    Belief b1 = GreaterThanThresholdBelief(d(0), embodiment->getManipulableSize(), eSpatial, certainty);
    Belief b2 = GreaterThanThresholdBelief(d(1), embodiment->getManipulableSize(), eSpatial, certainty);
    Belief b3 = GreaterThanThresholdBelief(d(2), embodiment->getManipulableSize(), eSpatial, certainty);

    return b1.logicOr(b2).logicOr(b3);
}

Belief PropertyBelief::Movable(const PrimitivePtr& primitive, const EmbodimentPtr& embodiment, float certainty)
{
    Eigen::Vector3f d = primitive->getDimensions();

    Belief b1 = LesserThanThresholdBelief(d(0), embodiment->getManipulableSize(), eSpatial, certainty);
    Belief b2 = LesserThanThresholdBelief(d(1), embodiment->getManipulableSize(), eSpatial, certainty);
    Belief b3 = LesserThanThresholdBelief(d(2), embodiment->getManipulableSize(), eSpatial, certainty);

    return b1.logicAnd(b2).logicAnd(b3);
}

Belief PropertyBelief::FeasibleBimanualDistance(const Eigen::Matrix4f& pose1, const Eigen::Matrix4f& pose2, const EmbodimentPtr& embodiment, float certainty)
{
    const float betaDmin = embodiment->getMinBimanualOperationDistance();
    const float betaDmax = embodiment->getMaxBimanualOperationDistance();

    // The distance between the two samples should lie in a certain interval
    return InThresholdIntervalBelief((pose1.block<3, 1>(0, 3) - pose2.block<3, 1>(0, 3)).norm(), betaDmin, betaDmax, eSpatial, certainty);
}

Belief PropertyBelief::FeasibleBimanualOrientation(const Eigen::Matrix4f& pose1, const Eigen::Matrix4f& pose2, const EmbodimentPtr& embodiment, float certainty)
{
    float epsilon = 0.2;

    float a = up.dot((pose1.block<3, 1>(0, 3) - pose2.block<3, 1>(0, 3)).normalized());
    return InThresholdIntervalBelief(a, -epsilon, epsilon, eRotational, certainty);
}

Belief PropertyBelief::Opposed(const Eigen::Matrix4f& pose1, const Eigen::Matrix4f& pose2, const EmbodimentPtr& embodiment, float certainty)
{
    float betaA = 0.1;

    // The two samples should approximately point into the opposite direction
    const Eigen::Vector3f& y1 = pose1.block<3, 1>(0, 1);
    const Eigen::Vector3f& y2 = pose2.block<3, 1>(0, 1);

    const Eigen::Vector3f& z1 = pose1.block<3, 1>(0, 2);
    const Eigen::Vector3f& z2 = pose2.block<3, 1>(0, 2);

    float a1 = z1.dot(-z2);
    float a2 = y1.dot(y2);
    float a3 = (pose2.block<3, 1>(0, 3) - pose1.block<3, 1>(0, 3)).normalized().dot(z1);

    CLAMP_TO_ACOS_DOMAIN(a1);
    CLAMP_TO_ACOS_DOMAIN(a2);
    CLAMP_TO_ACOS_DOMAIN(a3);

    Belief b1 = LesserThanThresholdBelief(acos(a1), betaA, eRotational, certainty);
    Belief b2 = LesserThanThresholdBelief(acos(a2), betaA, eRotational, certainty);
    Belief b3 = LesserThanThresholdBelief(acos(a3), betaA, eRotational, certainty);

    return b1.logicAnd(b2).logicAnd(b3);
}

Belief PropertyBelief::Aligned(const Eigen::Matrix4f& pose1, const Eigen::Matrix4f& pose2, const EmbodimentPtr& embodiment, float certainty)
{
    float betaA = 0.1;

    // The two samples should approximately point into the same direction
    const Eigen::Vector3f& y1 = pose1.block<3, 1>(0, 1);
    const Eigen::Vector3f& y2 = pose2.block<3, 1>(0, 1);

    const Eigen::Vector3f& z1 = pose1.block<3, 1>(0, 2);
    const Eigen::Vector3f& z2 = pose2.block<3, 1>(0, 2);

    float a1 = z1.dot(z2);
    float a2 = y1.dot(y2);

    CLAMP_TO_ACOS_DOMAIN(a1);
    CLAMP_TO_ACOS_DOMAIN(a2);

    Belief b1 = LesserThanThresholdBelief(acos(a1), betaA, eRotational, certainty);
    Belief b2 = LesserThanThresholdBelief(acos(a2), betaA, eRotational, certainty);

    return b1.logicAnd(b2);
}
