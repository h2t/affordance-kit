/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "Embodiment.h"

using namespace AffordanceKit;

Embodiment::Embodiment() :
    Embodiment(0, 0, 0, 0, 0, 0, 0)
{
}

Embodiment::Embodiment(float handBreadth, float handLength, float handSpan, float forehandLength, float minBimanualOperationDistance,
                       float maxBimanualOperationDistance, float manipulableSize) :
    handBreadth(handBreadth),
    handLength(handLength),
    handSpan(handSpan),
    forehandLength(forehandLength),
    minBimanualOperationDistance(minBimanualOperationDistance),
    maxBimanualOperationDistance(maxBimanualOperationDistance),
    manipulableSize(manipulableSize)
{
}

float Embodiment::getHandBreadth()
{
    return handBreadth;
}

void Embodiment::setHandBreadth(float value)
{
    handBreadth = value;
}

float Embodiment::getHandLength()
{
    return handLength;
}

void Embodiment::setHandLength(float value)
{
    handLength = value;
}

float Embodiment::getHandSpan()
{
    return handSpan;
}

void Embodiment::setHandSpan(float value)
{
    handSpan = value;
}

float Embodiment::getForehandLength()
{
    return forehandLength;
}

void Embodiment::setForehandLength(float value)
{
    forehandLength = value;
}

float Embodiment::getMinBimanualOperationDistance()
{
    return minBimanualOperationDistance;
}

void Embodiment::setMinBimanualOperationDistance(float value)
{
    minBimanualOperationDistance = value;
}

float Embodiment::getMaxBimanualOperationDistance()
{
    return maxBimanualOperationDistance;
}

void Embodiment::setMaxBimanualOperationDistance(float value)
{
    maxBimanualOperationDistance = value;
}

float Embodiment::getManipulableSize()
{
    return manipulableSize;
}

void Embodiment::setManipulableSize(float value)
{
    manipulableSize = value;
}
