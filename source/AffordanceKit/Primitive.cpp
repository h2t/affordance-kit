/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "Primitive.h"

#include <pcl/common/intersections.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>

using namespace AffordanceKit;

Primitive::Primitive() :
    id(boost::lexical_cast<std::string>(boost::uuids::random_generator()())),
    label(0),
    timestamp(0),
    identity(Eigen::Matrix4f::Identity())
{
}

void Primitive::sample(float spatialStepSize, int numOrientationalSteps, unsigned int maxPrimitiveSamplingSize)
{
    Eigen::MatrixXf S = sample_impl(spatialStepSize, numOrientationalSteps);
    setSampling(S);

    // Reset sampling if it turned out to be too large
    if (maxPrimitiveSamplingSize > 0 && getSamplingSize() > maxPrimitiveSamplingSize)
    {
        resetSampling();
    }
}

const Eigen::MatrixXf& Primitive::getSampling() const
{
    return sampling;
}

Eigen::MatrixXf Primitive::getSampling(float spatialStepSize, int numOrientationalSteps) const
{
    return sample_impl(spatialStepSize, numOrientationalSteps);
}

const Eigen::Ref<const Eigen::MatrixXf> Primitive::getSampling(unsigned int index) const
{
    if (4 * index > sampling.cols() - 4)
    {
        return identity.block(0, 0, 4, 4);
    }

    return sampling.block(0, 4 * index, 4, 4);
}

unsigned int Primitive::getSamplingSize() const
{
    return sampling.cols() / 4;
}

const std::string& Primitive::getId() const
{
    return id;
}

void Primitive::setId(const std::string& id)
{
    this->id = id;
}

void Primitive::setTimestamp(long long timestamp)
{
    this->timestamp = timestamp;
}

long long AffordanceKit::Primitive::getTimestamp() const
{
    return timestamp;
}

void Primitive::setLabel(uint32_t label)
{
    this->label = label;
}

uint32_t AffordanceKit::Primitive::getLabel() const
{
    return label;
}



void Primitive::setSampling(const Eigen::MatrixXf& sampling)
{
    this->sampling = sampling;
}

void Primitive::resetSampling()
{
    sampling = Eigen::MatrixXf(4, 0);
}
