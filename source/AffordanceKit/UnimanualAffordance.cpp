/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "UnimanualAffordance.h"

using namespace AffordanceKit;

UnimanualAffordance::UnimanualAffordance(const EmbodimentPtr& embodiment, const std::string& name, float affordanceExtractionCertainty) :
    Affordance(embodiment, name, affordanceExtractionCertainty)
{
    reset();
}

UnimanualAffordance::UnimanualAffordance() :
    Affordance()
{
    reset();
}

void UnimanualAffordance::evaluatePrimitiveSet(const PrimitiveSetPtr& primitiveSet)
{
    for (auto& primitive : *primitiveSet)
    {
        evaluatePrimitive(primitive);
    }
}

void UnimanualAffordance::evaluatePrimitive(const PrimitivePtr& primitive)
{
    PrimitivePair pp(primitive);

    if (theta->find(pp) != theta->end())
    {
        // No re-evaluation
        return;
    }

    preEvaluatePrimitive(primitive);

    unsigned int size = primitive->getSamplingSize();
    (*theta)[pp] = Eigen::MatrixXf(2, size);
    Eigen::MatrixXf& T = (*theta)[pp];

    #pragma omp parallel for
    for (unsigned int index = 0; index < size; index++)
    {
        T.col(index) = evaluateTheta(primitive, index).toVector();
    }
}

Eigen::Matrix4f UnimanualAffordance::getMostCertainPoseForPrimitive(const PrimitivePtr& primitive, Belief& value)
{
    PrimitivePair pp(primitive);

    if (theta->find(pp) == theta->end() || theta->at(pp).cols() == 0)
    {
        value = Belief(0, 0);
        return Eigen::Matrix4f::Identity();
    }

    // This implements a search for the greates beliefExists value
    Eigen::MatrixXf::Index maxRow, maxCol;
    theta->at(pp).row(0).maxCoeff(&maxRow, &maxCol);

    value = Belief(theta->at(pp).col(maxCol));
    return primitive->getSampling(maxCol);
}

BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::UnimanualAffordance)
