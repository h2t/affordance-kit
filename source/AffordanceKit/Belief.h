/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_AffordanceKit_Belief_H
#define _ARMARX_AffordanceKit_Belief_H

#include <memory>

#include <Eigen/Eigen>

namespace AffordanceKit
{
    class Belief
    {
    public:
        Belief() :
            m_exists(0),
            m_nexists(0),
            m_either(1)
        {
        }

        Belief(float ex, float nex, float certainty = 1.0) :
            m_exists(certainty* ex),
            m_nexists(certainty* nex),
            m_either(1 - m_exists - m_nexists)
        {
        }

        Belief(const Eigen::Vector2f& vector) :
            m_exists(vector(0)),
            m_nexists(vector(1)),
            m_either(1 - m_exists - m_nexists)
        {
        }

        float exists() const
        {
            return m_exists;
        }

        float nexists() const
        {
            return m_nexists;
        }

        float either() const
        {
            return m_either;
        }

        float beliefExists() const
        {
            return exists();
        }

        float beliefNexists() const
        {
            return nexists();
        }

        float beliefEither() const
        {
            return exists() + nexists() + either();
        }

        float plausibilityExists() const
        {
            return exists() + either();
        }

        float plausibilityNexists() const
        {
            return nexists() + either();
        }

        float plausibilityEither() const
        {
            return exists() + nexists() + either();
        }

        float expectedProbability() const
        {
            return (exists() - nexists() + 1) / 2;
        }

        float conflict() const
        {
            return exists() * nexists();
        }

        Belief operator*(const Belief& e)
        {
            float f_k = 1.0f / (1.0f - exists() * e.nexists() + nexists() * e.exists());

            return Belief(
                       f_k * (exists() * e.exists() + exists() * e.either() + either() * e.exists()),
                       f_k * (nexists() * e.nexists() + nexists() * e.either() + either() * e.nexists())
                   );
        }

        Belief operator*=(const Belief& e)
        {
            float f_k = 1.0f / (1.0f - exists() * e.nexists() + nexists() * e.exists());

            m_exists = f_k * (exists() * e.exists() + exists() * e.either() + either() * e.exists());
            m_nexists = f_k * (nexists() * e.nexists() + nexists() * e.either() + either() * e.nexists());
            m_either = 1 - m_exists - m_nexists;

            return *this;
        }

        Belief logicAnd(const Belief& e) const
        {
            return Belief(
                       m_exists * e.m_exists,
                       m_nexists + e.m_nexists - m_nexists * e.m_nexists
                   );
        }

        Belief logicOr(const Belief& e) const
        {
            return Belief(
                       m_exists + e.m_exists - m_exists * e.m_exists,
                       m_nexists * e.m_nexists
                   );
        }

        Belief logicNot(const Belief& e) const
        {
            return Belief(m_nexists, m_exists);
        }

        Belief addCertainty(float certainty) const
        {
            return Belief(certainty * m_exists, certainty * m_nexists);
        }

        Eigen::Vector2f toVector()
        {
            return Eigen::Vector2f(m_exists, m_nexists);
        }

        void fromVector(const Eigen::Vector2f& vector)
        {
            m_exists = vector(0);
            m_nexists = vector(1);
            m_either = 1 - m_exists - m_nexists;
        }

    protected:
        float m_exists;
        float m_nexists;
        float m_either;
    };

    typedef std::shared_ptr<Belief> BeliefPtr;
}

#endif
