/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Primitive_H
#define _AffordanceKit_Primitive_H

#include <VirtualRobot/MathTools.h>

#include <Eigen/Eigen>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

namespace AffordanceKit
{
    class PrimitiveSubVolume
    {
    public:
        PrimitiveSubVolume() :
            min(0, 0, 0),
            max(0, 0, 0),
            length(0, 0, 0),
            balanced(0, 0, 0)
        {
        }

        PrimitiveSubVolume(float min_x, float max_x, float min_y, float max_y, float min_z, float max_z) :
            min(min_x, min_y, min_z),
            max(max_x, max_y, max_z),
            length(max_x - min_x, max_y - min_y, max_z - min_z),
            balanced(2 * std::min(fabs(min_x), fabs(max_x)), 2 * std::min(fabs(min_y), fabs(max_y)), 2 * std::min(fabs(min_z), fabs(max_z)))
        {
        }

        const Eigen::Vector3f& getMin() const
        {
            return min;
        }
        const Eigen::Vector3f& getMax() const
        {
            return max;
        }
        const Eigen::Vector3f& getLength() const
        {
            return length;
        }
        const Eigen::Vector3f& getBalancedLength() const
        {
            return balanced;
        }

        bool isValid() const
        {
            return std::isfinite(min.x()) && std::isfinite(min.y()) && std::isfinite(min.z())
                   && std::isfinite(max.x()) && std::isfinite(max.y()) && std::isfinite(max.z());
        }

    protected:
        Eigen::Vector3f min;
        Eigen::Vector3f max;
        Eigen::Vector3f length;
        Eigen::Vector3f balanced;
    };

    typedef std::shared_ptr<PrimitiveSubVolume> PrimitiveSubVolumePtr;


    class Primitive
    {
    public:
        Primitive();

        virtual void sample(float spatialStepSize, int numOrientationalSteps, unsigned int maxPrimitiveSamplingSize = 0);
        virtual PrimitiveSubVolumePtr getVolumeInDirection(const Eigen::Matrix4f& pose, float depth = -1, float epsilon = 0.1) = 0;
        virtual Eigen::Vector3f getCenter() const = 0;
        virtual Eigen::Vector3f getDimensions() const = 0;
        virtual float getArea() const = 0;

        const Eigen::MatrixXf& getSampling() const;
        Eigen::MatrixXf getSampling(float spatialStepSize, int numOrientationalSteps) const;
        const Eigen::Ref<const Eigen::MatrixXf> getSampling(unsigned int index) const;
        unsigned int getSamplingSize() const;

        const std::string& getId() const;
        void setId(const std::string& id);

        void setTimestamp(long long timestamp);
        long long getTimestamp() const;

        virtual float isCircular() = 0;

        void setSampling(const Eigen::MatrixXf& sampling);
        void resetSampling();

        void setLabel(uint32_t label);
        uint32_t getLabel() const;

    protected:
        virtual Eigen::MatrixXf sample_impl(float spatialStepSize, int numOrientationalSteps) const = 0;

    protected:
        std::string id;
        uint32_t label;
        long long timestamp;

        Eigen::MatrixXf sampling;
        Eigen::MatrixXf identity;

    private:
        friend class boost::serialization::access;

        template<class Archive> void save(Archive& ar, const unsigned int version) const
        {
            unsigned int rows = sampling.rows();
            unsigned int cols = sampling.cols();

            ar& id& timestamp& rows& cols;

            for (unsigned int i = 0; i < rows; i++)
            {
                for (unsigned int j = 0; j < cols; j++)
                {
                    ar& sampling(i, j);
                }
            }
        }

        template<class Archive> void load(Archive& ar, const unsigned int version)
        {
            unsigned int rows, cols;
            ar& id& timestamp& rows& cols;

            Eigen::MatrixXf M(rows, cols);
            for (unsigned int i = 0; i < rows; i++)
            {
                for (unsigned int j = 0; j < cols; j++)
                {
                    ar& M(i, j);
                }
            }

            sampling = M;
        }

        BOOST_SERIALIZATION_SPLIT_MEMBER()
    };

    typedef std::shared_ptr<Primitive> PrimitivePtr;


    class PrimitivePair : public std::pair<PrimitivePtr, PrimitivePtr>
    {
    public:
        PrimitivePair(const PrimitivePtr& p1, const PrimitivePtr& p2)
        {
            if (p1->getId() < p2->getId())
            {
                first = p1;
                second = p2;
            }
            else
            {
                first = p2;
                second = p1;
            }
        }

        PrimitivePair(const PrimitivePtr& p)
        {
            first = second = p;
        }

        bool primitivesAreDifferent() const
        {
            return (first != second);
        }
    };

    typedef std::shared_ptr<PrimitivePair> PrimitivePairPtr;
}

#endif

