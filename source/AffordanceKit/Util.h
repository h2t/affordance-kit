/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Util_H
#define _AffordanceKit_Util_H

#define ZERO_TOLERANCE      1e-4

#define CLAMP_TO_ACOS_DOMAIN(var) { var = ((var > 1)? 1 : ((var < -1)? -1 : var)); }
#define MULTIPLY_RETURN_ZERO(var, value) {var *= value; if(var <= ZERO_TOLERANCE) { return Belief(); }}
#define MULTIPLY_IF_NONZERO(var, value) {if(var > ZERO_TOLERANCE) { var *= value; }}

#endif
