/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "Affordance.h"
#include "Exceptions.h"

using namespace AffordanceKit;

Affordance::Affordance() :
    Affordance(EmbodimentPtr(new Embodiment()), "")
{
}

Affordance::Affordance(const EmbodimentPtr& embodiment, const std::string& name, float affordanceExtractionCertainty) :
    timestamp(0),
    embodiment(embodiment),
    name(name),
    affordanceExtractionCertainty(affordanceExtractionCertainty)
{
}

void Affordance::setTimestamp(long timestamp)
{
    this->timestamp = timestamp;
}

long Affordance::getTimestamp() const
{
    return timestamp;
}

void Affordance::reset()
{
    theta.reset(new ThetaFunction());
}

const std::string& Affordance::getName() const
{
    return name;
}

void Affordance::removeForPrimitive(const PrimitivePtr& primitive)
{
    auto pair = PrimitivePair(primitive);
    if (theta->find(pair) != theta->end())
    {
        theta->erase(pair);
    }
}

void Affordance::addObservation(const ObservationPtr& observation)
{
    observations.push_back(observation);
}

const std::vector<ObservationPtr>& Affordance::getObservations()
{
    return observations;
}

float Affordance::getAffordanceExtractionCertainty() const
{
    return affordanceExtractionCertainty;
}

unsigned int Affordance::getNumPrimitives() const
{
    return theta->size();
}

void Affordance::setTheta(const ThetaFunctionPtr& theta)
{
    if (isValidThetaFunction(theta))
    {
        this->theta = theta;
    }
    else
    {
        throw InvalidInputException("Indices in theta functions must be unique and sorted");
    }
}

bool Affordance::isValidThetaFunction(const ThetaFunctionPtr& theta)
{
    // Make sure, the indices occur in sorted order
    for (auto& entry : *theta)
    {
        int currentIndex1 = -1;
        int currentIndex2 = -1;

        for (unsigned int i = 0; i < entry.second.cols(); i++)
        {
            if (currentIndex1 > 0 && currentIndex2 > 0)
            {
                if (currentIndex1 > entry.second(0, i))
                {
                    return false;
                }
                else if (currentIndex1 == entry.second(0, i) && currentIndex2 >= entry.second(1, i))
                {
                    return false;
                }
            }

            currentIndex1 = entry.second(0, i);
            currentIndex2 = entry.second(1, i);
        }
    }

    return true;
}

const ThetaFunctionPtr& Affordance::getTheta()
{
    return theta;
}

Belief Affordance::getTheta(const PrimitivePair& primitives, unsigned int index)
{
    Belief result = Belief(theta->at(primitives).col(index));

    for (auto& observation : observations)
    {
        result *= observation->query(primitives, index);
    }

    return result;
}

Belief Affordance::getTheta(const PrimitivePair& primitives, unsigned int index1, unsigned int index2)
{
    return Belief(theta->at(primitives).col(index1 * primitives.second->getSamplingSize() + index2));
}

const Eigen::MatrixXf& Affordance::getTheta(const PrimitivePair& primitives)
{
    return theta->at(primitives);
}

unsigned int Affordance::getThetaSize(const PrimitivePtr& primitive) const
{
    return theta->at(PrimitivePair(primitive)).cols();
}

unsigned int Affordance::getThetaSize(const PrimitivePair& primitives) const
{
    return theta->at(primitives).cols();
}

unsigned int Affordance::getThetaSize() const
{
    unsigned int result = 0;
    for (auto& entry : *theta)
    {
        result += getThetaSize(entry.first);
    }
    return result;
}

bool Affordance::existsForPrimitive(const PrimitivePtr& primitive)
{
    return (theta->find(PrimitivePair(primitive, primitive)) != theta->end());
}
