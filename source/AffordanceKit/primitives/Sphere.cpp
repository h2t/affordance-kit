/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "Sphere.h"
#include "../Util.h"

using namespace AffordanceKit;

Sphere::Sphere() :
    center(Eigen::Vector3f::Zero()),
    radius(0)
{
}

Sphere::Sphere(const Eigen::Vector3f& center, float radius) :
    center(center),
    radius(radius)
{
}

float Sphere::getRadius()
{
    return radius;
}

Eigen::MatrixXf Sphere::sample_impl(float spatialStepSize, int numOrientationalSteps) const
{
    // Sample at least in four directions
    int numSamplings = std::max(2 * M_PI * radius / spatialStepSize, 4.0);

    Eigen::Matrix4f P = Eigen::Matrix4f::Identity();
    P.block<3, 1>(0, 3) = center;

    Eigen::Matrix4f T = Eigen::Matrix4f::Identity();
    T(2, 3) = -radius - 5;

    Eigen::MatrixXf S(4, 4 * numSamplings * numSamplings * numOrientationalSteps);

    for (int i = 0; i < numSamplings; i++)
    {
        for (int j = 0; j < numSamplings; j++)
        {
            Eigen::Matrix4f R1 = Eigen::Matrix4f::Identity();
            R1.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(i * 2 * M_PI / numSamplings, Eigen::Vector3f::UnitX()));

            Eigen::Matrix4f R2 = Eigen::Matrix4f::Identity();
            R2.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(j * 2 * M_PI / numSamplings, Eigen::Vector3f::UnitY()));

            Eigen::Matrix4f R3 = Eigen::Matrix4f::Identity();
            for (int k = 0; k < numOrientationalSteps; k++)
            {
                R3.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(k * 2 * M_PI / numOrientationalSteps, Eigen::Vector3f::UnitZ()));
                S.block<4, 4>(0, 4 * (i * numSamplings * numOrientationalSteps + j * numOrientationalSteps + k)) = P * R1 * R2 * T * R3;
            }
        }
    }

    return S;
}

Eigen::Vector3f Sphere::getCenter() const
{
    return center;
}

Eigen::Vector3f Sphere::getDimensions() const
{
    return Eigen::Vector3f(2 * radius, 2 * radius, 2 * radius);
}

float Sphere::isCircular()
{
    return 0;
}

float AffordanceKit::Sphere::getArea() const
{
    return 4 * M_PI * radius * radius;
}

template<class Archive> void AffordanceKit::Sphere::save(Archive& ar, const unsigned int version) const
{
    ar& boost::serialization::base_object<Primitive>(*this);

    ar& center.x() & center.y() & center.z() & radius;
}

template<class Archive> void AffordanceKit::Sphere::load(Archive& ar, const unsigned int version)
{
    ar& boost::serialization::base_object<Primitive>(*this);

    float cx, cy, cz;
    ar& cx& cy& cz& radius;

    center = Eigen::Vector3f(cx, cy, cz);
}

BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::Sphere)
