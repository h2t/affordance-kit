/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Scene_H
#define _AffordanceKit_Scene_H

#include "Affordance.h"
#include "UnimanualAffordance.h"
#include "BimanualAffordance.h"
#include "PrimitiveSet.h"

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>

namespace AffordanceKit
{

    class Scene
    {
    public:
        Scene();
        Scene(const std::string& filename);
        Scene(const AffordanceKit::PrimitiveSetPtr& primitives, const std::vector<UnimanualAffordancePtr>& unimanualAffordances, const std::vector<BimanualAffordancePtr>& bimanualAffordances);

        const std::vector<UnimanualAffordancePtr>& getUnimanualAffordances() const;
        const std::vector<BimanualAffordancePtr>& getBimanualAffordances() const;
        const std::vector<AffordancePtr>& getAffordances() const;

        std::vector<AffordancePtr> getAffordancesByName(const std::string& name);

        PrimitiveSetPtr getPrimitives();
        const PrimitiveSetPtr getPrimitives() const;

        /*!
         * Remove affordances from the scene for which the maximum expected probability value does not exceed
         * a minimum threshold
         */
        void applyMinExpectedProbabilityFilter(float minExpectedProbability);

        /*!
         * Serializes the scene content and writes it to a file.
         *
         * \param filename  Specifies the file to export to
         */
        void save(const std::string& filename) const;

        /*!
         * Constructs a scene from an exported scene serialization
         *
         * \param filename  Specifies the file to import from
         */
        void load(const std::string& filename);

    public:
        // Methods for generating scene statistics

        /*!
         * Computes the total uncertainty, i.e. the sum of the uncertainty for each affordance for each sampling,
         * normalized to the maximum possible uncertainty.
         *
         * \param affordanceName  Restricts computation to particular affordance types
         */
        float getTotalUncertainty(const std::string& affordanceName = "");

        /*!
         * Computes the positive decision rate, i.e. the amount of samplings with predominantly positive evidence
         *
         * \param affordanceName  Restricts computation to particular affordance types
         */
        float getPositiveDecisionRate(const std::string& affordanceName = "");

        /*!
         * Computes the negative decision rate, i.e. the amount of samplings with predominantly negative evidence
         *
         * \param affordanceName  Restricts computation to particular affordance types
         */
        float getNegativeDecisionRate(const std::string& affordanceName = "");

        float getTotalConflict(const std::string& affordanceName = "");

    protected:
        std::vector<UnimanualAffordancePtr> unimanualAffordances;
        std::vector<BimanualAffordancePtr> bimanualAffordances;
        std::vector<AffordancePtr> affordances;

        PrimitiveSetPtr primitives;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& primitives;
            ar& unimanualAffordances;
            ar& bimanualAffordances;
            ar& affordances;
        }
    };
    using ScenePtr = std::shared_ptr<Scene>;
}

#endif

