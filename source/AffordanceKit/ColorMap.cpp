/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ColorMap.h"

using namespace AffordanceKit;

Eigen::Vector3i AffordanceKit::ColorMap::GetVisualizationColor(const AffordanceKit::Belief& belief)
{
    float min_hue = 0;
    float max_hue = 115;

    float min_light = 0.3;
    float max_light = 1.0;

    float hue = min_hue + belief.expectedProbability() * (max_hue - min_hue);

    float saturation = 1;

    float lightness = min_light + belief.either() * (max_light - min_light);

    return hsl2rgb(hue, saturation, lightness);
}

Eigen::Vector3i ColorMap::hsl2rgb(float hue, float saturation, float lightness)
{
    float chroma = (1 - fabs(2 * lightness - 1)) * saturation;
    float h_prime = hue / 60;
    float X = chroma * (1 - fabs(fmod(h_prime, 2) - 1));

    Eigen::Vector3f color(0, 0, 0);
    if (h_prime >= 0 && h_prime <= 1)
    {
        color << chroma, X, 0;
    }
    else if (h_prime >= 1 && h_prime <= 2)
    {
        color << X, chroma, 0;
    }
    else if (h_prime >= 2 && h_prime <= 3)
    {
        color << 0, chroma, X;
    }
    else if (h_prime >= 3 && h_prime <= 4)
    {
        color << 0, X, chroma;
    }
    else if (h_prime >= 4 && h_prime <= 5)
    {
        color << X, 0, chroma;
    }
    else if (h_prime >= 5 && h_prime <= 6)
    {
        color << chroma, 0, X;
    }

    float m = lightness - 0.5 * chroma;
    return Eigen::Vector3i((unsigned char)(255 * (color(0) + m)), (unsigned char)(255 * (color(1) + m)), (unsigned char)(255 * (color(2) + m)));
}
