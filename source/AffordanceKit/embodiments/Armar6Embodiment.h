/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Armar6Embodiment_H
#define _AffordanceKit_Armar6Embodiment_H

#include "../Embodiment.h"

namespace AffordanceKit
{

    class Armar6Embodiment : public Embodiment
    {
    public:
        Armar6Embodiment() :
            Embodiment(130, 230, 100, 120, 400, 800, 700)
        {
        }

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<Embodiment>(*this);
        }
    };

    typedef std::shared_ptr<Armar6Embodiment> Armar6EmbodimentPtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::Armar6Embodiment)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::Armar6Embodiment)

#endif
