/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Embodiment_H
#define _AffordanceKit_Embodiment_H

#include <Eigen/Eigen>

#include <memory>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>

namespace AffordanceKit
{
    /**
     * @brief The Embodiment class specifies parameters of the robot embodiment.
     *
     * The embodiment parameters directly influence the affordance detection procedure, particularly for the detection of
     * grasp affordances. The parameters are:
     *
     *  - Hand breadth
     *  - Hand length
     *  - Hand span
     *  - Forehand length
     *  - Minimum bimanual operation distance
     *  - Maximum bimanual operation distance
     *  - Manipulable size
     */
    class Embodiment
    {
    public:
        Embodiment();
        Embodiment(float handBreadth, float handLength, float handSpan, float forehandLength, float minBimanualOperationDistance,
                   float maxBimanualOperationDistance, float manipulableSize);

        float getHandBreadth();
        void setHandBreadth(float value);

        float getHandLength();
        void setHandLength(float value);

        float getHandSpan();
        void setHandSpan(float value);

        float getForehandLength();
        void setForehandLength(float value);

        float getMinBimanualOperationDistance();
        void setMinBimanualOperationDistance(float value);

        float getMaxBimanualOperationDistance();
        void setMaxBimanualOperationDistance(float value);

        float getManipulableSize();
        void setManipulableSize(float value);

    protected:
        float handBreadth;
        float handLength;
        float handSpan;
        float forehandLength;
        float minBimanualOperationDistance;
        float maxBimanualOperationDistance;
        float manipulableSize;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& handBreadth& handLength& handSpan& forehandLength& minBimanualOperationDistance& maxBimanualOperationDistance& manipulableSize;
        }
    };

    typedef std::shared_ptr<Embodiment> EmbodimentPtr;

}

#endif


