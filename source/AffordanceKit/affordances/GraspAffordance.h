/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_GraspAffordance_H
#define _AffordanceKit_GraspAffordance_H

#include "../UnimanualAffordance.h"
#include "../PropertyBelief.h"
#include "PlatformGraspAffordance.h"
#include "PrismaticGraspAffordance.h"

namespace AffordanceKit
{

    class GraspAffordance : public UnimanualAffordance
    {
    public:
        GraspAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                        const PlatformGraspAffordancePtr& platformGraspAffordance = PlatformGraspAffordancePtr(new PlatformGraspAffordance()),
                        const PrismaticGraspAffordancePtr& prismaticGraspAffordance = PrismaticGraspAffordancePtr(new PrismaticGraspAffordance)) :
            UnimanualAffordance(embodiment, "G"),
            platformGraspAffordance(platformGraspAffordance),
            prismaticGraspAffordance(prismaticGraspAffordance)
        {
        }

        Belief evaluateTheta(const PrimitivePtr& primitive, unsigned int index) const override
        {
            Belief lowerLevelAffordance1 = platformGraspAffordance->getTheta(primitive, index);
            Belief lowerLevelAffordance2 = prismaticGraspAffordance->getTheta(primitive, index);

            return lowerLevelAffordance1.logicOr(lowerLevelAffordance2);

        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            platformGraspAffordance->evaluatePrimitive(primitive);
            prismaticGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        PlatformGraspAffordancePtr platformGraspAffordance;
        PrismaticGraspAffordancePtr prismaticGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<UnimanualAffordance>(*this);
            ar& platformGraspAffordance& prismaticGraspAffordance;
        }
    };

    typedef std::shared_ptr<GraspAffordance> GraspAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::GraspAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::GraspAffordance)

#endif




