/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_BimanualLiftAffordance_H
#define _AffordanceKit_BimanualLiftAffordance_H

#include "../BimanualAffordance.h"
#include "../PropertyBelief.h"
#include "BimanualPrismaticGraspAffordance.h"
#include "BimanualOpposedPrismaticGraspAffordance.h"
#include "BimanualOpposedPlatformGraspAffordance.h"

namespace AffordanceKit
{

    class BimanualLiftAffordance : public BimanualAffordance
    {
    public:
        BimanualLiftAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                               const BimanualPrismaticGraspAffordancePtr& prismaticGraspAffordance = BimanualPrismaticGraspAffordancePtr(new BimanualPrismaticGraspAffordance()),
                               const BimanualOpposedPrismaticGraspAffordancePtr& opposedPrismaticGraspAffordance = BimanualOpposedPrismaticGraspAffordancePtr(new BimanualOpposedPrismaticGraspAffordance()),
                               const BimanualOpposedPlatformGraspAffordancePtr& opposedPlatformGraspAffordance = BimanualOpposedPlatformGraspAffordancePtr(new BimanualOpposedPlatformGraspAffordance())) :
            BimanualAffordance(embodiment, "Bi-Lf"),
            prismaticGraspAffordance(prismaticGraspAffordance),
            opposedPrismaticGraspAffordance(opposedPrismaticGraspAffordance),
            opposedPlatformGraspAffordance(opposedPlatformGraspAffordance)
        {
        }

    protected:
        Belief evaluateTheta(const PrimitivePair& primitives, unsigned int samplingIndex1, unsigned int samplingIndex2) const override
        {
            if (primitives.primitivesAreDifferent())
            {
                return Belief();
            }

            // The two samples should allow one of the two prismatic grasp types
            //Belief b1 = prismaticGraspAffordance->query(primitives.first, samplingIndex1, samplingIndex2);
            Belief b2 = opposedPrismaticGraspAffordance->getTheta(primitives.first, samplingIndex1, samplingIndex2);
            Belief b3 = opposedPlatformGraspAffordance->getTheta(primitives.first, samplingIndex1, samplingIndex2);
            Belief b_horizontal = PropertyBelief::Horizontal(Eigen::Vector3f(primitives.first->getSampling(samplingIndex1).block<3, 1>(0, 3) - primitives.second->getSampling(samplingIndex2).block<3, 1>(0, 3)), embodiment);

            return (b2.logicOr(b3)).logicAnd(b_horizontal);
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            prismaticGraspAffordance->evaluatePrimitive(primitive);
            opposedPrismaticGraspAffordance->evaluatePrimitive(primitive);
            opposedPlatformGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        BimanualPrismaticGraspAffordancePtr prismaticGraspAffordance;
        BimanualOpposedPrismaticGraspAffordancePtr opposedPrismaticGraspAffordance;
        BimanualOpposedPlatformGraspAffordancePtr opposedPlatformGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<BimanualAffordance>(*this);
            ar& prismaticGraspAffordance& opposedPrismaticGraspAffordance& opposedPlatformGraspAffordance;
        }
    };

    typedef std::shared_ptr<BimanualLiftAffordance> BimanualLiftAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::BimanualLiftAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::BimanualLiftAffordance)

#endif



