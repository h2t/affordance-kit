/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_PrismaticGraspAffordance_H
#define _AffordanceKit_PrismaticGraspAffordance_H

#include "../UnimanualAffordance.h"
#include "../PropertyBelief.h"

namespace AffordanceKit
{

    class PrismaticGraspAffordance : public UnimanualAffordance
    {
    public:
        PrismaticGraspAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()), float affordanceExtractionCertainty = 1.0f) :
            UnimanualAffordance(embodiment, "G-Pr", affordanceExtractionCertainty)
        {
        }

        Belief evaluateTheta(const PrimitivePtr& primitive, unsigned int index) const override
        {
            PrimitiveSubVolumePtr v = primitive->getVolumeInDirection(primitive->getSampling(index), embodiment->getForehandLength());

            // Space in x-direction should be larger than hand breadth
            Belief b1 = PropertyBelief::GreaterThanThresholdBelief(v->getBalancedLength().x(), embodiment->getHandBreadth(), PropertyBelief::eSpatial);

            // Space in y-direction should fit into hand span
            Belief b2 = PropertyBelief::LesserThanThresholdBelief(v->getLength().y(), embodiment->getHandSpan(), PropertyBelief::eSpatial);

            return b1.logicAnd(b2).addCertainty(getAffordanceExtractionCertainty());
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
        }

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<UnimanualAffordance>(*this);
        }
    };

    typedef std::shared_ptr<PrismaticGraspAffordance> PrismaticGraspAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::PrismaticGraspAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::PrismaticGraspAffordance)

#endif


