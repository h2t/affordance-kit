/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_AlignedPlatformGraspAffordance_H
#define _AffordanceKit_AlignedPlatformGraspAffordance_H

#include "../BimanualAffordance.h"
#include "../PropertyBelief.h"
#include "BimanualGraspAffordance.h"

#include <memory>

namespace AffordanceKit
{

    class BimanualAlignedGraspAffordance : public BimanualAffordance
    {
    public:
        BimanualAlignedGraspAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                                       const BimanualGraspAffordancePtr& bimanualGraspAffordance = BimanualGraspAffordancePtr(new BimanualGraspAffordance())) :
            BimanualAffordance(embodiment, "Bi-Al-G"),
            bimanualGraspAffordance(bimanualGraspAffordance)
        {
        }

    protected:
        Belief evaluateTheta(const PrimitivePair& primitives, unsigned int samplingIndex1, unsigned int samplingIndex2) const override
        {
            const Eigen::Matrix4f& pose1 = primitives.first->getSampling(samplingIndex1);
            const Eigen::Matrix4f& pose2 = primitives.second->getSampling(samplingIndex2);

            Belief lowerLevelAffordance = bimanualGraspAffordance->getTheta(primitives, samplingIndex1, samplingIndex2);
            Belief b_orientation = PropertyBelief::FeasibleBimanualOrientation(pose1, pose2, embodiment);
            Belief b_aligned = PropertyBelief::Aligned(pose1, pose2, embodiment);

            return lowerLevelAffordance.logicAnd(b_orientation).logicAnd(b_aligned);
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            bimanualGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        BimanualGraspAffordancePtr bimanualGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<BimanualAffordance>(*this);
            ar& bimanualGraspAffordance;
        }
    };

    typedef std::shared_ptr<BimanualAlignedGraspAffordance> BimanualAlignedGraspAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::BimanualAlignedGraspAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::BimanualAlignedGraspAffordance)

#endif




