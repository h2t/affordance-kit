/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_LiftAffordance_H
#define _AffordanceKit_LiftAffordance_H

#include "../UnimanualAffordance.h"
#include "../PropertyBelief.h"
#include "PrismaticGraspAffordance.h"

namespace AffordanceKit
{

    class LiftAffordance : public UnimanualAffordance
    {
    public:
        LiftAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                       const PrismaticGraspAffordancePtr& prismaticGraspAffordance = PrismaticGraspAffordancePtr(new PrismaticGraspAffordance())) :
            UnimanualAffordance(embodiment, "Lf"),
            prismaticGraspAffordance(prismaticGraspAffordance)
        {
        }

        Belief evaluateTheta(const PrimitivePtr& primitive, unsigned int index) const override
        {
            Belief b_movable = PropertyBelief::Movable(primitive, embodiment);
            Belief b_prismatic = prismaticGraspAffordance->getTheta(primitive, index);

            return b_prismatic.logicAnd(b_movable);
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            prismaticGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        PrismaticGraspAffordancePtr prismaticGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<UnimanualAffordance>(*this);
            ar& prismaticGraspAffordance;
        }
    };

    typedef std::shared_ptr<LiftAffordance> LiftAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::LiftAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::LiftAffordance)

#endif


