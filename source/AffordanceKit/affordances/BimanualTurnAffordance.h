/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_BimanualTurnAffordance_H
#define _AffordanceKit_BimanualTurnAffordance_H

#include "../BimanualAffordance.h"
#include "../PropertyBelief.h"
#include "BimanualOpposedPrismaticGraspAffordance.h"

namespace AffordanceKit
{

    class BimanualTurnAffordance : public BimanualAffordance
    {
    public:
        BimanualTurnAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                               const BimanualOpposedPrismaticGraspAffordancePtr& bimanualOpposedPrismaticGraspAffordance = BimanualOpposedPrismaticGraspAffordancePtr(new BimanualOpposedPrismaticGraspAffordance())) :
            BimanualAffordance(embodiment, "Bi-Tn"),
            bimanualOpposedPrismaticGraspAffordance(bimanualOpposedPrismaticGraspAffordance)
        {
        }

    protected:
        Belief evaluateTheta(const PrimitivePair& primitives, unsigned int samplingIndex1, unsigned int samplingIndex2) const override
        {
            if (primitives.primitivesAreDifferent())
            {
                return Belief();
            }

            // The two samples should allow an opposed prismatic grasp
            Belief lowerLevelAffordance = bimanualOpposedPrismaticGraspAffordance->getTheta(primitives, samplingIndex1, samplingIndex2);

            // The primitive shape should be approximately circular
            Belief circular = PropertyBelief::Circular(primitives.first);

            return lowerLevelAffordance.logicAnd(circular);
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            bimanualOpposedPrismaticGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        BimanualOpposedPrismaticGraspAffordancePtr bimanualOpposedPrismaticGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<BimanualAffordance>(*this);
            ar& bimanualOpposedPrismaticGraspAffordance;
        }
    };

    typedef std::shared_ptr<BimanualTurnAffordance> BimanualTurnAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::BimanualTurnAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::BimanualTurnAffordance)

#endif




